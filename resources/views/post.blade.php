@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>{{ $post->title }}</h1>

                @if(!empty($post->image))
                <section class="my-4 mb-5">
                    <img src="/storage/{{ $post->image }}" class="img-fluid nice-image">
                </section>
                @endif

                <article class="mt-3">
                    {!! $post->body !!}
                </article>

                <hr class="mt-4">

                <section class="similar-posts-list mt-4">
                    <h3>Kiti straipsniai</h3>
                    <div class="news-list">
                    @foreach($similarPosts as $post)
                        <a href="{{ route('post', [ 'slug'=> $post->slug, 'id' => $post->id ]) }}"
                           class="news-item-block p-3 mb-3">
                            <h2 class="news-item-header m-0 p-0 mb-2">
                                {{ $post->title }}
                            </h2>
                            <small class="py-3">
                                {{ $post->excerpt }}
                            </small>
                            <span class="news-item-timestamp">
                            {{ $post->created_at->timezone('Europe/Vilnius') }}
                        </span>
                        </a>
                    @endforeach
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection