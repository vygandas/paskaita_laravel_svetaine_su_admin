@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="mb-5">Naujienos!</h1>

                <div class="news-list">


                    @foreach($posts as $post)
                    <a href="{{ route('post', [ 'slug'=> $post->slug, 'id' => $post->id ]) }}"
                       class="news-item-block p-3 mb-3">
                        <h2 class="news-item-header m-0 p-0 mb-2">
                            {{ $post->title }}
                        </h2>
                        <img src="/storage/{{ $post->image }}" class="img-fluid">
                        <small class="py-3">
                            {{ $post->excerpt }}
                        </small>
                        <span class="news-item-timestamp">
                            {{ $post->created_at->timezone('Europe/Vilnius') }}
                        </span>
                    </a>
                    @endforeach



                </div>

            </div>
        </div>
    </div>
@endsection