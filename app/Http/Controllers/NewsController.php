<?php
namespace App\Http\Controllers;


use App\Post;

class NewsController extends Controller
{

    public function index() {
        $posts = Post::get();
        return view('news')
            ->with('posts', $posts);
    }

    public function getPost($id, $slug = null) {
        $post = Post::where('id', $id)->first();

        \Debugbar::error('Error!');

        // Random rikiavimas https://laravel.com/docs/5.6/queries#ordering-grouping-limit-and-offset
        $similarPosts = Post::inRandomOrder()
            ->where('id', '<>', $id)
            ->take(3)
            ->get();

        return view('post')
            ->with('post', $post)
            ->with('similarPosts', $similarPosts);
    }

}