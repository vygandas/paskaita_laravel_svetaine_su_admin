<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/naujienos', 'NewsController@index')->name('news');
Route::get('/apie', 'InfoController@about')->name('about');

Route::get('/{id}/{slug?}', 'NewsController@getPost')->name('post');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
